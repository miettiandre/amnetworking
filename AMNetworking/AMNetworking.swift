//
//  AMNetworking.swift
//  AMNetworking
//
//  Created by andre mietti on 14/02/22.
//

import UIKit

public final class AMNetworking {

    public init() {}

    func makeRequest() {

        let url = URL(string: "https://bit.ly/3sspdFO")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = URLSession.shared.dataTask(with: url) { data, response, error in

            guard error == nil else {
                print("Invalid Response \(String(describing: error?.localizedDescription))")
                return
            }

            debugPrint("Success: \(String(describing: response))")

        }

        task.resume()

    }

}
